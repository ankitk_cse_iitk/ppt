Require Import List.
Import ListNotations.

Section Stream.
  Variable A : Type.
  CoInductive stream :=
  | Yield : A  -> stream -> stream
  .

  Fixpoint take (s : stream)(n : nat) : list A :=
  match s,n with
    | Yield a sP, S m => a :: take sP m
    | _         , 0   => []
  end.

  CoInductive stream_eq : stream -> stream -> Prop :=
  | YieldEq : forall (h : A)(r s : stream), stream_eq r s -> stream_eq (Yield h r) (Yield h s).



  Definition hd (s : stream) : A :=
    match s with
      | Yield a _ => a
    end.

  Definition tl (s : stream) : stream :=
    match s with
      | Yield _ sp => sp
    end.

End Stream.

Arguments Yield [A] _ _.
Arguments YieldEq [A] _ _ _ _.
Arguments take [A] _ _.
Arguments stream_eq [A] _ _.
Arguments hd [A] _.
Arguments tl [A] _.

Section Map.
  Variable A B : Type.

  CoFixpoint mapS (f : A -> B)(s : stream A) : stream B :=
    match s with
      | Yield a sP => Yield (f a) (mapS f sP)
    end.
End Map.

Arguments mapS [A B] _ _.

CoFixpoint zeros := Yield 0 zeros.
CoFixpoint infinite {A : Type} (a : A) : stream A := Yield a (infinite a).

Definition ones := infinite 1.
Definition onesP := mapS (plus 1) zeros.

Print zeros.


Theorem increment_zeros_is_ones : stream_eq ones onesP.
  cofix. assumption.
  Show Proof.
Abort.



Section Stream_eq_coind.

  Variable A : Type.

  Variable P : stream A -> stream A -> Prop.

  Hypothesis HeadCase : forall r s,  P r s -> hd r = hd s.

  Hypothesis TailCase : forall r s, P r s -> P (tl r) (tl s).

  Theorem stream_eq_coind : forall r s, P r s -> stream_eq r s.
    cofix; destruct r; destruct s; intro.
    generalize (HeadCase (Yield a r) (Yield a0 s)); intro Heq.
    simpl in Heq. rewrite Heq. constructor.
    apply stream_eq_coind.
    apply (TailCase (Yield a r) (Yield a0 s) H). exact H.
  Qed.

End Stream_eq_coind.

Arguments stream_eq_coind [A] _ _ _ _ _ _.


Ltac crush :=
  repeat match goal with
           | [ H : ?T      |- ?T     ]  => exact H
           | [ H : _ /\ _  |- _      ]  => destruct H
           | [             |- _ /\ _  ]  => refine (conj _ _)
           | [ H : _ = _   |- _  ]  => rewrite H
           | [             |- forall _ : stream _, _] => intro
           | [             |- _ -> _                ] => intro

           | _                      => trivial
         end.

Theorem ones_eq : stream_eq ones onesP.
  apply (stream_eq_coind (fun s1 s2 => s1 = ones /\ s2 = onesP)); crush.
Qed.
