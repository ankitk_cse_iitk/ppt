Print well_founded.
Print Acc.
Print Fix.
Require Import Streams.

Ltac handleNot := let H := fresh "contraHyp" in intro H; inversion H.
Ltac crush :=
  repeat match goal with
           | [ H : ?T |- ?T ]     => exact H
           | [ |- not ?T     ]    => handleNot
           | [ |- Acc _ _ -> _ ]  => intro
           | [ |- ?T -> False  ]  => assert T
           | [ H : Acc _ _ |- _ ] => induction H 
           | _ => idtac
         end.

Section InfDecChain.
  Variable A : Type.
  Variable R : A -> A -> Prop.
  
                 
  CoInductive infDecChain : Stream A -> Prop :=
  | ChanCons : forall x y s,  infDecChain (Cons y s) -> R y x -> infDecChain (Cons x (Cons y s)).

  Lemma noetherian : forall x, Acc R x -> forall s, not (infDecChain (Cons x s)).
  Proof.
    intro x.
    crush.
    intro s.
    crush.
    refine (H0 y _ _ H3); crush.
  Qed.    
End InfDecChain.

